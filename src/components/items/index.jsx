import React from "react";
import styled from "styled-components";

const Items = ({ setPlayerOneItem, setPlayerTwoItem, win }) => {
  const itemsList = ["Pedra", "Papel", "Tesoura"];

  const handleOnClick = (event) => {
    if (!win) {
      setPlayerOneItem(event.target.value);
      setTimeout(() => {
        setPlayerTwoItem(itemsList[Math.floor(Math.random() * 3)]);
      }, 500);
    }
  };

  return (
    <Button>
      <ButtonBox onClick={handleOnClick} value="Pedra">
        Pedra
      </ButtonBox>
      <ButtonBox onClick={handleOnClick} value="Papel">
        Papel
      </ButtonBox>
      <ButtonBox onClick={handleOnClick} value="Tesoura">
        Tesoura
      </ButtonBox>
    </Button>
  );
};

export default Items;

const Button = styled.div`
  display: flex;
`;

const ButtonBox = styled.button`
  background: none;
  border: 1px solid white;
  color: white;
  font-size: 1.6rem;
  border-radius: 18px;
  outline: 0;
  margin: 10px 20px;
  cursor: pointer;
  padding: 14px 30px;
`;
