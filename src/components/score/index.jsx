import React from "react";
import styled from "styled-components";

const Score = ({ scoreList }) => {
  return (
    <Box>
      <p style={{ margin: "0" }}>Placar</p>
      <PlayersBox>
        <Player>
          <ScoreNum>{scoreList[0]}</ScoreNum>
          <Text>Jogador</Text>
        </Player>
        <Player>
          <ScoreNum>{scoreList[1]}</ScoreNum>
          <Text>Máquina</Text>
        </Player>
      </PlayersBox>
    </Box>
  );
};

export default Score;

const Box = styled.div`
  border: 2px solid black;
  border-radius: 8px;
  padding: 40px;
`;

const PlayersBox = styled.div`
  display: flex;
  justify-content: center;
  margin-top: 20px;
`;

const Player = styled.div`
  margin: 0 25px;
`;

const ScoreNum = styled.p`
  font-size: 3rem;
  margin: 0;
`;

const Text = styled.p`
  font-size: 16px;
  margin: 0;
`;
