import React from "react";
import styled from "styled-components";

const Board = ({ playerOneItem, playerTwoItem }) => {
  return (
    <Body>
      <Player>
        <Text>Máquina</Text>
        {playerOneItem}
      </Player>

      <Line></Line>

      <Player>
        <Text>Jogador</Text>
        {playerTwoItem}
      </Player>
    </Body>
  );
};

export default Board;

const Player = styled.div`
  position: relative;
  display: flex;
  align-items: center;
  justify-content: center;
  height: 36px;
`;

const Text = styled.p`
  position: absolute;
  left: 0;
  font-size: 12px;
  margin: 0;
`;

const Line = styled.div`
  border-top: 3px solid black;
  width: 500px;
`;

const Body = styled.div`
  margin: 60px 40px;
`;
