import React from "react";
import styled from "styled-components";

const GameOver = ({
  winner,
  setWinner,
  setPlayerOneItem,
  setPlayerTwoItem,
}) => {
  const handleOnClick = () => {
    setWinner(false);
    setPlayerOneItem("");
    setPlayerTwoItem("");
  };
  return (
    <Body>
      <Winner>{winner === "Jogador" ? "Você ganhou!" : "Você perdeu!"}</Winner>
      <Retry onClick={handleOnClick}>Jogar novamente</Retry>
    </Body>
  );
};

export default GameOver;

const Winner = styled.p`
  font-size: 1.8rem;
`;

const Retry = styled.button`
  background: none;
  border: 1px solid white;
  color: white;
  font-size: 1rem;
  border-radius: 18px;
  outline: 0;
  margin: 10px 20px;
  cursor: pointer;
  padding: 10px 26px;
`;

const Body = styled.div`
  margin: 60px 40px;
`;
