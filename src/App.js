import React, { useState, useEffect } from "react";
import { Score, Board, Items, GameOver } from "./components";
import "./App.css";

function App() {
  const [playerOneItem, setPlayerOneItem] = useState("");
  const [playerTwoItem, setPlayerTwoItem] = useState("");
  const [scoreList, setScoreList] = useState([0, 0]);
  const [winner, setWinner] = useState(false);
  const winConditions = [
    ["Pedra", "Tesoura"],
    ["Tesoura", "Papel"],
    ["Papel", "Pedra"],
  ];

  useEffect(() => {
    if (
      winConditions.some(
        (arr) => arr[0] === playerOneItem && arr[1] === playerTwoItem
      )
    ) {
      setScoreList([scoreList[0] + 1, scoreList[1]]);
      setTimeout(() => {
        setWinner("Jogador");
      }, 1000);
    } else if (
      winConditions.some(
        (arr) => arr[0] === playerTwoItem && arr[1] === playerOneItem
      )
    ) {
      setScoreList([scoreList[0], scoreList[1] + 1]);
      setTimeout(() => {
        setWinner("Máquina");
      }, 1000);
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [playerOneItem, playerTwoItem]);

  return (
    <div className="App">
      <Score scoreList={scoreList} />
      {winner ? (
        <GameOver
          winner={winner}
          setWinner={setWinner}
          setPlayerOneItem={setPlayerOneItem}
          setPlayerTwoItem={setPlayerTwoItem}
        />
      ) : (
        <Board
          playerOneItem={playerOneItem}
          playerTwoItem={playerTwoItem}
          setScoreList={setScoreList}
        />
      )}

      <Items
        setPlayerOneItem={setPlayerOneItem}
        setPlayerTwoItem={setPlayerTwoItem}
        win={winner}
      />
    </div>
  );
}

export default App;
